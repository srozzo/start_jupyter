#!/bin/sh 
## Repo for dockerfiles https://github.com/jupyter/docker-stacks

# Image name 
IMAGE=jupyter/datascience-notebook
IMAGE_VERSION=${1:-latest}

# Comtainer name 
CONTAINER_NAME=datascience

get_script_path() {
    cd "$(dirname "${0}")" || return 1
    echo "$(pwd -P)"

    return 0
}

kill_running_container() {
    RUNNING=$(docker ps -q -f name=${CONTAINER_NAME})
    if [ "${RUNNING}" ]; then
        docker kill "${RUNNING}" > /dev/null
        ret=$?
        return $ret
    fi
    return 0
}

delete_container() {
    EXISTING_CONTAINER=$(docker ps -aq -f status=exited -f status=created -f name=${CONTAINER_NAME})
    if [ "${EXISTING_CONTAINER}" ]; then
        docker rm ${CONTAINER_NAME} > /dev/null
        ret=$?
        return $ret
    fi
    return 0
}

start_container() {
    CONTAINER_ID=$(docker run --rm -p 8888:8888 --name "${CONTAINER_NAME}" -v "$(get_script_path)":/home/jovyan -d "${IMAGE}":"${IMAGE_VERSION}")
    # Need to sleep here to ensure the image has started
    sleep 3
    NOTEBOOK_URL=$(docker exec "${CONTAINER_ID}" jupyter notebook list | grep -oE '(^http.*)' | awk '{print $1}')
    open "${NOTEBOOK_URL}"
    return $? 
}

kill_running_container
delete_container
start_container